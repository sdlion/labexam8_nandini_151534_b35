<?php
use App\ProfilePicture\ProfilePicture;
require_once("../../../vendor/autoload.php");

$pic = new ProfilePicture;

$allData =  $pic->trashitem('obj');

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Atomic Project</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lobster|Merriweather|Montserrat|Shrikhand" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="../https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<div class="container-fluid wrapper">

    </div>


            <div class="col-md-10 ">
                <div class="atomic-nav">
                    <div class="navbar">
                        <div class="container">
                            <div class="navbar-header">

                            </div>
                            <div class="btn-group-lg nav navbar-nav" role="group" aria-label="...">
                                <a href="create.php" class="navbar-btn btn btn-info">Add Item</a>
                                <a href="trash_item.php" class="navbar-btn btn btn-warning">Trash Item</a>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-12 ">
                    <?php

                    $serial =1;

                    echo "<table class=\"table table-bordered table-hover my-table-border my-td\">";

                    echo "<tr><th>Serial</th><th>ID</th><th>Name</th><th>Picture</th><th>Action</th></tr>";

                    foreach($allData as $oneData){

                        echo "<tr>";
                        echo "<td>$serial</td>";
                        echo "<td>$oneData->id</td>";
                        echo "<td>$oneData->name</td>";
                        echo "<td><img src='$oneData->profile_image' alt='' width='200'></td>";
                        echo "<td><a href='view.php?id=$oneData->id'><button class=\"btn btn-info btn-sm\">View</button></a><br>
<a href='edit.php?id=$oneData->id'><button class=\"btn btn-primary btn-sm\">Edit</button></a><br>
<a href='delete.php?id=$oneData->id'><button class=\"btn btn-danger btn-sm\">Delete</button></a><br>
<a href='trash.php?id=$oneData->id'> <button class=\"btn btn-warning btn-sm\">Trash</button>
                </a></td>";
                        echo "</tr>";
                        $serial++;
                    }



                    ?>
                </div>



            </div>
        </div>
    </div>
    <hr class="hr-divider">
    <div class="footer">
        <div class="row">

            <div class="user-img col-md-6">
            </div>
        </div>
    </div>
</div>




<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>



</body>

</html>

