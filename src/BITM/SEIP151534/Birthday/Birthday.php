<?php
namespace App\Birthday;
use App\Message\Message;
use App\Model\database as db;
use App\Utility\Utility;
use PDO;

//require_once("../../../../vendor/autoload.php");
class Birthday extends db
{
    public $id;
    public $name;
    public $dob;

    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data = Null)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];

        }
        if (array_key_exists('name', $data)) {
            $this->name= $data['name'];

        }
        if (array_key_exists('dob', $data)) {
            $this->dob = $data['dob'];

        }

    }
    public function store(){
        $arrData=array($this->name,$this->dob);

        $sql= "Insert INTO birthday(name,dob) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);

        if($result)
            Message::setMessage("Sucess!data has been inserted sucessfully");
        else
            Message::setMessage("Failure!data has not been inserted sucessfully");

        Utility::redirect('create.php');
    }// end of store method
    public function index($fetchMode='ASSOC'){
        $sql = "SELECT * from birthday where is_deleted = 0 ";

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();
    public function view($fetchMode='ASSOC'){
        $sql='SELECT * from birthday WHERE id='.$this->id;
        $STH = $this->DBH->query($sql);
        //echo $sql;


        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();


    public function update(){
        $arrData=array($this->name,$this->dob);

        //UPDATE `atomic_project_35`.`book_title` SET `book_title` = 'nan' WHERE `book_title`.`id` = 4;

        $sql="update birthday SET  name=? ,dob=? WHERE id=".$this->id;
        $STH =$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');
    }//end of update();

    public function delete(){
        $sql="Delete from birthday where id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');

    }//end of delete();

    public function trash(){

        $sql = "Update birthday SET is_deleted=NOW()where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()


}